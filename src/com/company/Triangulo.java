package com.company;

import java.util.ArrayList;

public class Triangulo {

    public void validarTriangulo(ArrayList<Integer> lado){

        if (((lado.get(0) + lado.get(1)) > lado.get(2)) &&
                ((lado.get(0) + lado.get(2)) > lado.get(1)) &&
                ((lado.get(1) + lado.get(2)) > lado.get(0))){

            CalculoArea areaTriangulo = new CalculoArea();
            areaTriangulo.calcular(lado);

        } else {

            System.out.println("Triângulo inválido!");
        }
    }

}