package com.company;

import java.util.ArrayList;

public class CalculoArea {

    double area;

    public void calcular(ArrayList<Integer> lado){
        if(lado.size() == 3){
            double sub = (lado.get(0) + lado.get(1) + lado.get(2)) / 2;
            area = Math.sqrt(sub * (sub - lado.get(0)) * (sub - lado.get(1)) * (sub - lado.get(2)));

        } else if (lado.size() == 2){
            area = lado.get(0) * lado.get(1);
        } else {
            area = Math.pow(lado.get(0),2) * Math.PI;
        }

        System.out.println("A área da forma geométrica é : " + area);
    }
}
