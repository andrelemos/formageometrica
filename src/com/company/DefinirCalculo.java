package com.company;

import java.util.ArrayList;

public class DefinirCalculo {

    public void definirCalculo(ArrayList<Integer> lado) {
        if (lado.size() == 3) {
            Triangulo triangulo = new Triangulo();
            triangulo.validarTriangulo(lado);

        } else {
            CalculoArea area = new CalculoArea();
            area.calcular(lado);
        }
    }
}