package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class FormaGeometrica {

    ArrayList<Integer> lado = new ArrayList<>();

    public void informaLados(){
        Scanner scanner = new Scanner(System.in);
        String controle = "S";
        int num = 0;
        int forma;

        System.out.println("Digite a quantidade de lados para cálculo: ");
        System.out.println("(1 - círculo; 2 - retângulo; 3 - triângulo)");
        forma = scanner.nextInt();

        if ( forma > 3){
            System.out.println("ERRO! Não é permitido mais de 3 lados.");

        } else {
            do {
                if (lado.size() < forma) {

                    System.out.println("Digite o tamanho do lado: ");
                    lado.add(scanner.nextInt());
                    num++;
                }

            } while (num < forma);
        }
        if (forma < 4){
            DefinirCalculo calculo = new DefinirCalculo();
            calculo.definirCalculo(lado);
        }

    }

}